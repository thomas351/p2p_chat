# P2P Chat

## Description

To start this project off, we copy the [rust-libp2p chat example](https://github.com/libp2p/rust-libp2p/tree/master/examples/chat):

A basic chat application with logs demonstrating libp2p and the gossipsub protocol combined with mDNS for the discovery of peers to gossip with.
It showcases how peers can connect, discover each other using mDNS, and engage in real-time chat sessions.

## Research

* 2023-03-07 till 2024-03-13 [Go+Js+Rust interoperable chat example](https://github.com/libp2p/universal-connectivity)
* 2021-01 Go Chat using Libp2p: [forum thread](https://discuss.libp2p.io/t/working-p2p-chat-application/795)
* 2019-12-02 Javascript Chat using Libp2p: [article](https://medium.com/swlh/building-a-chat-application-using-libp2p-7c2f990ed473), [tutorial](https://www.simpleaswater.com/chat-using-libp2p/#build-a-libp2p-bundle)

## Usage

1. Using two terminal windows, start two instances, typing the following in each:

    ```sh
    cargo run
    ```

1. Mutual mDNS discovery may take a few seconds. When each peer does discover the other
it will print a message like:

    ```sh
    mDNS discovered a new peer: {peerId}
    ```

1. Type a message and hit return: the message is sent and printed in the other terminal.

1. Close with `Ctrl-c`. You can open more terminal windows and add more peers using the same line above.

When a new peer is discovered through mDNS, it can join the conversation, and all peers will receive messages sent by that peer.
If a participant exits the application using `Ctrl-c` or any other method, the remaining peers will receive an mDNS expired event and remove the expired peer from their list of known peers.

## Conclusion

This chat application demonstrates the usage of **libp2p** and the gossipsub protocol for building a decentralized chat system.
By leveraging mDNS for peer discovery, users can easily connect with other peers and engage in real-time conversations.
The example provides a starting point for developing more sophisticated chat applications using **libp2p** and exploring the capabilities of decentralized communication.
